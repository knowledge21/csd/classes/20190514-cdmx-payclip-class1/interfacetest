package br.com.k21;

import junit.framework.Assert;
import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ClipDashboardTest extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/flaviocpontes/chromedriver");
		return new ChromeDriver();
	}
	
	@Test
	public void loginTest() throws InterruptedException {
		goTo("https://dashboard.clip.mx/");
		fill("input[name=email]").with("kumar@payclip.com");
		fill("input[name=password]").with("Kumar02");
		click("button[type=submit]");
		
		await().atMost(5000).until( ".css-kdnz8a").isPresent();
		
		Assert.assertEquals("Cerrar Sesión", $(".css-kdnz8a.ejiiu0b0 span:nth-child(2)").getText());

		////*[@id="root"]/div/div[1]/header/div/div[2]/div/span[2]
	}

	@Test
	public void failedLoginTest() throws InterruptedException {
		goTo("https://dashboard.clip.mx/");
		fill("input[name=email]").with("kumarerror@error.com");
		fill("input[name=password]").with("Kumar02");
		click("button[type=submit]");

		await().atMost(10000).until( ".jss370.jss373.css-1asdqwx.e3n14fs3").isPresent();

		Assert.assertEquals("Correo electrónico o contraseña no válidos", $(".jss370.jss373.css-1asdqwx.e3n14fs3").getText());

		////*[@id="root"]/div/div[1]/header/div/div[2]/div/span[2]
	}
}
